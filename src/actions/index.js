import {FETCH_WEATHER, ROOT_URL,FILTER_DATA} from '../consts/constants';
import axios from 'axios';

export function fetchWeather(city) {
    const url= `${ROOT_URL}&q=${city},us`
    const request =axios.get(url);

    return {
        type:FETCH_WEATHER,
        payload:request
    }
}

export function filterWeather(filterText) {
    return{
        type: FILTER_DATA,
        text: filterText
    }
}