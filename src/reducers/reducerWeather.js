import {FETCH_WEATHER,FILTER_DATA} from '../consts/constants'

export function WeatherReducer(state=[],action){
    switch (action.type){
        case FETCH_WEATHER:
            return [action.payload.data,...state];
    }

    return state;
}

export function FilterWeatherReducer(state=[],action){
    switch (action.type) {
        case FILTER_DATA:
            return Object.assign({}, state, {
                filteredData: action.text
            });
    }
    return state;
}