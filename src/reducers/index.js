import { combineReducers } from 'redux';
import {WeatherReducer,FilterWeatherReducer} from './reducerWeather';


const rootReducer = combineReducers({
  weather: WeatherReducer,
  filteredData: FilterWeatherReducer
});

export default rootReducer;
