import React,{Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import compose from 'recompose/compose';
import {fetchWeather} from "../actions";
import {filterWeather} from '../actions';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class SearchBar extends Component {

    constructor(props){
        super(props);
        this.state={
            term: '',
            filteredText:''
        }
    }
    onInputChange=(event)=>{
        this.setState({term:event.target.value});
    }

    onFilterChange=(event)=>{
        this.setState({filteredText:event.target.value});
    }

    onSubmit=(event)=>{
        event.preventDefault();
        this.props.fetchWeather(this.state.term);
        this.setState({term:''});
    }
    onFilterButtonClicked=(event)=>{
        this.props.filterWeather(this.state.filteredText)
    }


    render(){
        const { classes } = this.props;
        return (
            <div>
            <form onSubmit={this.onSubmit}>
                <TextField
                    id="name"
                    placeholder="Enter city name"
                    className={classes.textField}
                    value={this.state.term}
                    onChange={this.onInputChange}
                    margin="normal"
                />

                <span className="input-group-btn">
                    <Button variant="contained"
                            className={classes.button}
                            type="submit"
                            color="primary">Search</Button>
                </span>
            </form>
                <TextField
                    className="input-group"
                    placeholder="enter filter Name"
                    onChange={this.onFilterChange}/>
                <Button variant="contained"
                        className={classes.button} color="secondary"
                        onClick={this.onFilterButtonClicked}>FILTER</Button>
          </div>
        )
    }
}



function mapDispatchToProps(dispatch){
    return {...bindActionCreators({fetchWeather,filterWeather},dispatch)};
}

export default compose(
    withStyles(styles),
    connect(null, mapDispatchToProps)
)(SearchBar);

