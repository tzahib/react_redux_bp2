import React, {Component} from 'react';
import {connect} from 'react-redux';


class WeatherList extends Component {
    constructor(props){
        super(props);
    }

    renderWeather=(item)=>{
        return (
            <tr key={item.city.id}>
                <td>{item.city.name}</td>
            </tr>
        )
    }

    render(){
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Temprature</th>
                        <th>Pressure</th>
                        <th>Humidty</th>
                    </tr>
                </thead>
                <tbody>
                {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }
}

function mapStateToProps(state){
    let weatherData=state.weather;
    if(typeof state!=='undefined' &&
        typeof state.weather &&
        typeof state.filteredData!=='undefined' &&
        typeof state.filteredData.length==='undefined'){
        let filterData=state.filteredData.filteredData.toLowerCase();
        weatherData=weatherData.filter((item)=> {
            let cityName=item.city.name.toLowerCase();
            console.log(`city Name = ${cityName} filteredData = ${filterData} indexOf = ${cityName.indexOf(filterData)}`);
            if(cityName.indexOf(filterData) > -1)
                return item;
        });
    }
    return { weather:weatherData};
}

export default connect(mapStateToProps)(WeatherList);
